
This module runs with the Tryton application platform.

Installing
----------

See INSTALL

Note
----

This module is developed and tested over a Tryton server and core modules
patched with [trytond-patches](https://gitlab.com/m9s/trytond-patches).
Maybe some of these patches are required for the module to work.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/account_tax_rule_country/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

   * http://bugs.tryton.org/
   * http://groups.tryton.org/
   * http://wiki.tryton.org/
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

